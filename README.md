# Passenger service

## Docker

### Configure
Set the proper platform.

```shell
passenger-service % export DOCKER_DEFAULT_PLATFORM=linux/amd64
```

### Build image

```shell
passenger-service % export DOCKER_DEFAULT_PLATFORM=linux/amd64
passenger-service % docker build -t com.github.juanmougan.examples/passenger-service .
```
